function [mask] = detector(index,testImages,threshold,stepSize, W, B)
%DETECTOR Summary of this function goes here
%   Detailed explanation goes here


img = imread(char(testImages(index)));
%imgGround = imread(char(ground_truths(index)));


img = rgb2gray(im2single(img));

imgWidth = size(img, 2);
imgHeight = size(img, 1);

mask = zeros(imgHeight, imgWidth, 1);
scoreList = [];

imshow(img);
hold on;
for i = 1:stepSize:imgHeight
    for j = 1:stepSize:imgWidth
        
        % Then, from the help:
        hRectangle = rectangle('Position',[j - 0.5, i - 0.5,stepSize, stepSize],...
          'EdgeColor', 'r',...
          'LineWidth', 2,...
          'LineStyle','-');
      
        middleForm = (stepSize / 2) + 0.5;
        jMid = (j + middleForm) - 1;
        iMid = (i + middleForm) - 1;
        
        middle = rectangle('Position',[jMid - 0.5, iMid - 0.5, 1, 1],...
          'EdgeColor', 'r',...
          'LineWidth', 2,...
          'LineStyle','-');
        
        %imgTemp = imcrop(img, [j, i, stepSize - 1, stepSize - 1]);
        imgTemp = img(i:i+(stepSize-1),j:j+(stepSize-1));
        
        hog = vl_hog(imgTemp, stepSize);
        
        % Check ground truth, store hog in said array
        
        % run hog  on section, run if statement to see what array 
        % to store value in.
        features_pos(1, :) = reshape(hog, 1, 31);
        
        scores = features_pos * W + B;
        
        %scores = scores * -1;
        
        if scores > threshold
            %fish = [fish; features_pos];
            set(hRectangle, 'EdgeColor', 'g')
            set(middle, 'EdgeColor', 'g')
            
            mask(i,j) = 1;
            
        end
        
        scoreList = [scoreList; scores];
        
        
        %Append hog into value
        
%         imhog = vl_hog('render', hog, 'verbose') ; % Visualise
% 
%         clf ; imagesc(imhog) ; colormap gray ; % shows gradiants
        
        %
        %pause(0);
        
        delete(hRectangle)
        
    end
end


end

