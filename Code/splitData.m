function [trainImages,  testImages, trainGround, testGround] = splitData(images,ground_truths, testSplit)
%SPLITDATA Summary of this function goes here
%   Detailed explanation goes here

% Find out how many rows we need to randomize.
numberOfRows = size(images, 1);

newRowOrder = randperm(numberOfRows);

images = images(newRowOrder, :);
ground_truths = ground_truths(newRowOrder, :);

% split data 70/30
trainPerc = round((testSplit/100) * numberOfRows);

trainImages = images(1:trainPerc,:); % splits data up to testSplit
testImages = images(trainPerc+1:end,:); % runs from testSplit to end

trainGround = ground_truths(1:trainPerc,:);
testGround = ground_truths(trainPerc+1:end,:);




end

