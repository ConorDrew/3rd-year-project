function [image_url,image_ground] = get_image_paths(data_path)

    images = dir( fullfile(data_path,'images/*.png'));
    images_ground = dir( fullfile(data_path,'truth_foreground/*.png'));

    image_url = cell(size(images,1), 1);
    image_ground = cell(size(images,1),1);

    for i = 1:size(images,1)

        image_url{i} = fullfile(data_path, 'images/', images(i).name);
        image_ground{i} = fullfile(data_path, 'truth_foreground/', images_ground(i).name);
    end
    
    
end

