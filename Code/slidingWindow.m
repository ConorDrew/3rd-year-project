function [fish,background] = slidingWindow(image,groundImage, stepSize, visual, delay)
%SLIDINGWINDOW Summary of this function goes here
%   Detailed explanation goes here


img = image;
imgGround = groundImage;

img = rgb2gray(im2single(img));


imgWidth = size(img, 2);
imgHeight = size(img, 1);

count = 1;

fish = [];
background = [];


if strcmp(visual, 'true')
    
    
    imshow(img);
    hold on;
    for i = 1:stepSize:imgHeight
        for j = 1:stepSize:imgWidth

            % Then, from the help:
            hRectangle = rectangle('Position',[j - 0.5, i - 0.5,stepSize, stepSize],...
              'EdgeColor', 'r',...
              'LineWidth', 2,...
              'LineStyle','-');

            middleForm = (stepSize / 2) + 0.5;
            jMid = (j + middleForm) - 1;
            iMid = (i + middleForm) - 1;

            middle = rectangle('Position',[jMid - 0.5, iMid - 0.5, 1, 1],...
              'EdgeColor', 'r',...
              'LineWidth', 2,...
              'LineStyle','-');

            %imgTemp = imcrop(img, [j, i, stepSize - 1, stepSize - 1]);
            imgTemp = img(i:i+(stepSize-1),j:j+(stepSize-1));

            hog = vl_hog(imgTemp, stepSize);

            % Check ground truth, store hog in said array

            % run hog  on section, run if statement to see what array 
            % to store value in.
            features_pos(1, :) = reshape(hog, 1, 31);

            if imgGround(iMid,jMid) > 0
                fish = [fish; features_pos];
                set(hRectangle, 'EdgeColor', 'g')
                set(middle, 'EdgeColor', 'g')
            elseif imgGround(iMid,jMid) == 0
                background = [background; features_pos];
            end

            %Append hog into value

    %         imhog = vl_hog('render', hog, 'verbose') ; % Visualise
    % 
    %         clf ; imagesc(imhog) ; colormap gray ; % shows gradiants
            
            if delay > 0
                pause(delay);
            end

            

            delete(hRectangle)
            count = count + 1;

        end
    end
    
elseif strcmp(visual, 'false')
    for i = 1:stepSize:imgHeight
        for j = 1:stepSize:imgWidth
            
            middleForm = (stepSize / 2) + 0.5;
            jMid = (j + middleForm) - 1;
            iMid = (i + middleForm) - 1;

            %imgTemp = imcrop(img, [j, i, stepSize - 1, stepSize - 1]);
            imgTemp = img(i:i+(stepSize-1),j:j+(stepSize-1));

            hog = vl_hog(imgTemp, stepSize);

            % Check ground truth, store hog in said array

            % run hog  on section, run if statement to see what array 
            % to store value in.
            features_pos(1, :) = reshape(hog, 1, 31);

            if imgGround(iMid,jMid) > 0
                fish = [fish; features_pos];
            elseif imgGround(iMid,jMid) == 0
                background = [background; features_pos];
            end

        end
    end
else
    disp("ERROR")
    return
end

end

