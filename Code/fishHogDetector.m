% Reqirements
% fishHogDetector
% detector
% get_Image_Paths
% splitData
% slidingWindow
% report_accuracy
% finishBeeps

close all
clear
clc

%% values that would be passed in.
if ~exist('splitData.mat', 'file')
    
    data_path = '../data/SD/';

    [images, ground_truths] = get_image_paths(data_path);

    %% Split Data
    % splits data by x amount, CHANGE TO  STRUCT FOR OUTPUTS.

    
    
    [trainImages, testImages, trainGround, testGround] = splitData(images, ground_truths, 70);
    
    
    save('splitData.mat', 'trainImages', 'testImages','trainGround','testGround')

else
    load('splitData.mat');
end

%% Gets Feats for background and fish.
if ~exist('features.mat', 'file')
    stepSize =  5;

    [allFish,allBackground] = hogFeats(trainImages,trainGround,stepSize);
    
    save('features.mat', 'allFish', 'allBackground')
else
    load('features.mat');
end


%% balance/ merge Data


featBackground = allBackground(1:size(allFish,1),:);

feats = [allFish; featBackground];
labels = [ones(size(allFish,1),1); -1*ones(size(featBackground,1),1)];

%% SVM 
LAMBDA = 0.00001;
numIterations = 1000000;

[W, B] = vl_svmtrain(feats', labels, LAMBDA,...
                                    'MaxNumIterations', numIterations);
                                
confidences = [allFish; featBackground]*W + B;

[tp_rate, fp_rate, tn_rate, fn_rate] =  report_accuracy( confidences, labels );

%% Detector

index = 2; % change to test diffrent images
threshold = 0.4;
stepSize = 5;

mask = detector(index,testImages,threshold,stepSize, W, B);

