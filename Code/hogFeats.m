function [allFish,allBackground] = hogFeats(trainImages,trainGround, stepSize)
    numImages = size(trainImages,1);
    allFish = [];
    allBackground = [];
    counter = 0;
    %stepSize = 5;
    disp('Getting HOG features');
    for i = 1:numImages 
        if (counter==14)
            fprintf('%5.0f%% Complete.\n', (i/numImages)*100);
            counter = 0;
        end
        img = imread(char(trainImages(i)));
        imgGround = imread(char(trainGround(i)));

        [fish, background] = slidingWindow(img, imgGround, stepSize, 'false', 0.25);

        allFish = [allFish; fish];
        allBackground = [allBackground; background];

        counter = counter + 1;

    end
    disp('COMPLETE');
end

